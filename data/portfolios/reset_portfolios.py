import os, glob, shutil

def reset_jsons():
    for file in glob.glob("*.json"):
        backup_file = file + ".bak"
        if (os.path.exists(backup_file)):
            os.remove(file)
            shutil.copyfile(backup_file, file)

if __name__ == "__main__":
    reset_jsons()