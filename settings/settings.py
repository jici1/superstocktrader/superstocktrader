from utils.file_operations import read_json

def get_settings(settings_file):
    return read_json(settings_file)["settings"][0]

