from settings.settings import get_settings
import pytest as pt

def test_get_settings():
    setting_file = "settings/settings.json"
    settings = get_settings(setting_file)

    assert settings.get("TestSettings")=="SuperTest"