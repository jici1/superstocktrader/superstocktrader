# Stock trading program


Notes to developers  
Naming conventions:

We use python offical naming convention
https://www.python.org/dev/peps/pep-0008/

TLDR:
Mostly snakecase:
https://en.wikipedia.org/wiki/Snake_case

Code quality  
Your code must:
* Be DRY [(Don't Repeat Yourself)](https://en.wikipedia.org/wiki/Don't_repeat_yourself)
* Be Tested!
* Every commit must pass all test!
* As much as possible, follow [the SOLID principles for classes](https://en.wikipedia.org/wiki/SOLID)
* Be as simple as possible! [(KISS)](https://en.wikipedia.org/wiki/KISS_principle)
* Forbid [broken window and minimise software entropy](https://blog.codinghorror.com/the-broken-window-theory/)   


Using the Gitlab interface we shall create task that follow the [Design by contract approach](https://en.wikipedia.org/wiki/Design_by_contract)

We shall also draw on intention and logic on a schema. Examples can be see in the schema sub directory. The software [DrawIO](https://app.diagrams.net/) is recommended.    
It can be used to draw logic flow, UML class diagram or any other worthwhile ideas!