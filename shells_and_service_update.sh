#!/bin/sh
cd /home/jc/superstocktrader

systemctl stop super_stock_trader.service
systemctl stop update_repo.timer 
systemctl stop update_repo.service 
systemctl stop restart_service.service 
systemctl stop restart_service.timer 

chmod 777 update_repo.sh
chmod 777 restart_service.sh

sudo cp super_stock_trader.service /lib/systemd/system/super_stock_trader.service
sudo cp update_repo.service /lib/systemd/system/update_repo.service 
sudo cp update_repo.timer /lib/systemd/system/update_repo.timer 
sudo cp restart_service.timer /lib/systemd/system/restart_service.timer 
sudo cp restart_service.service /lib/systemd/system/restart_service.service 

sudo chmod 777 /lib/systemd/system/super_stock_trader.service
sudo chmod 777 /lib/systemd/system/update_repo.service 
sudo chmod 777 /lib/systemd/system/update_repo.timer 
sudo chmod 777 /lib/systemd/system/restart_service.timer 
sudo chmod 777 /lib/systemd/system/restart_service.service 
systemctl daemon-reload 

systemctl restart super_stock_trader.service
systemctl restart update_repo.timer 
systemctl restart restart_service.timer 

systemctl enable super_stock_trader.service
systemctl enable update_repo.timer 
systemctl enable restart_service.timer 

#systemctl status super_stock_trader.service
systemctl status update_repo.timer 