import pytest
from stock_trading_manager import *
from settings.settings import get_settings

settings_for_test = get_settings("settings/settings_tests_stock_trading_manager.json")

def test_init_traders():
    traders = init_traders(settings_for_test)
    assert len(traders) == 2
    trader1 = traders[0]
    assert trader1.name == "Trader1_test"
    assert trader1.StockToWatch == ["TSLA", "SHOP"]
    assert trader1.Algo_to_use== ["Lowest_30_days"]

def test_market_is_open():
    closed_time = time(7,30)
    closed_date = datetime(2020, 5, 24)

    openned_time = time(13,00)
    openned_date = datetime(2020, 5, 27)
    holiday_date = datetime(2020, 5, 25)

    assert market_is_open(closed_time,closed_date) == False
    assert market_is_open(openned_time,openned_date) == True
    assert market_is_open(openned_time,holiday_date) == False

if __name__ =="__main__":
    test_init_traders()