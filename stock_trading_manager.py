from settings.settings import get_settings
import time as t
from trading_utils.trader import Trader
from trading_utils.market_state import *
from datetime import time, date, datetime
from trading_utils.Reporter.day_top_report import DayTopReport
from trading_utils.Stock_Analyst.stock_analyst import get_one_year_high

#initialisation
def init_traders(settings):
    traders = settings.get("Traders")
    trader_list = []
    for trader in traders:
        current_trader = Trader(trader)
        current_trader.init()
        trader_list.append(current_trader)
    return trader_list

def run_traders(settings, trader_list):
    is_open = False
    is_opening = False
    is_closing = False

    simulate_market_hours = settings["Simulation"]["SimulateMarketData"]

    if (simulate_market_hours):
        begin_time = datetime.now()
        market_time, market_date = make_market_date_time(settings["Simulation"]["SimulationTime"])

    while True:
        if (simulate_market_hours):

            new_check_is_open = market_is_open(market_time, market_date)

            is_opening, is_closing = get_market_state(is_open,new_check_is_open)
            
            if is_closing:
                DayTopReport()

            is_open = new_check_is_open

            market_time, market_date = update_simulated_time(market_time, market_date, begin_time)

        else:
            new_check_is_open = market_is_open()

            is_opening, is_closing = get_market_state(is_open,new_check_is_open)

            if is_closing:
                DayTopReport()

            is_open = new_check_is_open

        if (not(is_open)):
            t.sleep(30)
            print("nothing is happening!!")
            continue

        for trader in trader_list:
            trader.trade(is_opening, is_closing)
        t.sleep(settings.get("SleepTimeBetweenCycle"))

if __name__ == "__main__":

    settings = get_settings("settings/settings.json")
    trader_list = init_traders(settings)
    run_traders(settings, trader_list)