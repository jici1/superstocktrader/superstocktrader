from yahoo_fin import stock_info as si
import pandas as pd
from datetime import datetime, time, timedelta
from trading_utils.stock_data_api import *

def get_one_year_high(stock_symbol):
    __check_arguments_validity__(stock_symbol=stock_symbol)

    quote_table = si.get_quote_table(stock_symbol)
    one_year_range = quote_table['52 Week Range']
    one_year_high = one_year_range.split(" - ")[1]

    return price_string_to_float(one_year_high)

def get_one_year_low(stock_symbol):
    __check_arguments_validity__(stock_symbol=stock_symbol)

    quote_table = si.get_quote_table(stock_symbol)
    one_year_range = quote_table['52 Week Range']
    one_year_low = one_year_range.split(" - ")[0]

    return price_string_to_float(one_year_low)

def price_string_to_float(price_string):

    price_float = ''
    for position in price_string:
        if position == ",":
            continue
        price_float += position

    return float(price_float)

def get_top_gainers(size):
    __check_arguments_validity__(size=size)

    return si.get_day_gainers()[0:size]
    
def get_top_losers(size):
    __check_arguments_validity__(size=size)

    return si.get_day_losers()[0:size]

def price_is_lowest_in_x_days(stock_symbol, number_of_days):
    __check_arguments_validity__(stock_symbol=stock_symbol, number_of_days=number_of_days)

    lowest_price = get_lowest_price_in_x_days(stock_symbol, number_of_days)

    return bool(get_current_stock_value(stock_symbol) < lowest_price)

def get_lowest_price_in_x_days(stock_symbol, number_of_days):
    __check_arguments_validity__(stock_symbol=stock_symbol, number_of_days=number_of_days)

    now = datetime.now().date()
    start_date = now - timedelta(number_of_days)

    data = si.get_data(stock_symbol, start_date, now)
    lowest_price = min(data["low"])
    return lowest_price

def get_highest_price_in_x_days(stock_symbol, number_of_days):
    __check_arguments_validity__(stock_symbol=stock_symbol, number_of_days=number_of_days)

    now = datetime.now().date()
    start_date = now - timedelta(number_of_days)

    data = si.get_data(stock_symbol, start_date, now)
    highest_price = max(data["high"])
    return highest_price

def __check_arguments_validity__(size=1, stock_symbol="VALID", number_of_days=1):

    if not isinstance(size, int):
        raise TypeError("Size must be an integer")
    if isinstance(size,int) and size <= 0:
        raise ValueError("Size must be greater than 0")

    if not isinstance(stock_symbol, str):
        raise TypeError("stock_symbol must be a string")

    if not isinstance(number_of_days, int):
        raise TypeError("number_of_days must be an integer")
    if isinstance(number_of_days, int) and number_of_days <= 0:
        raise ValueError("number_of_days must be greater than 0")

