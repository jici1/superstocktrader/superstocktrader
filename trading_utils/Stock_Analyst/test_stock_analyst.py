import pytest
import stock_analyst as sa

def test_get_one_year_high():
    assert sa.get_one_year_high("TSLA") > 0 

    with pytest.raises(TypeError):
        sa.get_one_year_high(2)

def test_get_one_year_low():
    assert sa.get_one_year_low("TSLA") > 0 

    with pytest.raises(TypeError):
        sa.get_one_year_low(2)

def test_price_string_to_float():
    assert sa.price_string_to_float("123,456,789.99") == 123456789.99

def test_get_top_gainers():
    
    assert len(sa.get_top_gainers(10)) == 10
    assert len(sa.get_top_gainers(20)) == 20

    with pytest.raises(TypeError):
        sa.get_top_gainers("test")
    with pytest.raises(ValueError):
        sa.get_top_gainers(0)
    with pytest.raises(ValueError):
        sa.get_top_gainers(-5)

def test_get_top_losers():

    assert len(sa.get_top_losers(10)) == 10
    assert len(sa.get_top_losers(20)) == 20

    with pytest.raises(TypeError):
        sa.get_top_losers("test")
    with pytest.raises(ValueError):
        sa.get_top_losers(0)
    with pytest.raises(ValueError):
        sa.get_top_losers(-5)

def test_price_is_lowest_in_x_days():

    assert isinstance(sa.price_is_lowest_in_x_days("TSLA", 20), bool)

    with pytest.raises(TypeError):
        sa.price_is_lowest_in_x_days(2, 20)
    with pytest.raises(TypeError):
        sa.price_is_lowest_in_x_days("TSLA", "test")
    with pytest.raises(ValueError):
        sa.price_is_lowest_in_x_days("TSLA", 0)
    with pytest.raises(ValueError):
        sa.price_is_lowest_in_x_days("TSLA", -5)

def test_get_lowest_price_in_x_days():

    assert isinstance(sa.get_lowest_price_in_x_days("TSLA", 100), float)

    with pytest.raises(TypeError):
        sa.get_lowest_price_in_x_days(2, 20)
    with pytest.raises(TypeError):
        sa.get_lowest_price_in_x_days("TSLA", "test")
    with pytest.raises(ValueError):
        sa.get_lowest_price_in_x_days("TSLA", 0)
    with pytest.raises(ValueError):
        sa.get_lowest_price_in_x_days("TSLA", -5)

def test_get_highest_price_in_x_days():

    assert isinstance(sa.get_highest_price_in_x_days("TSLA", 100), float)

    with pytest.raises(TypeError):
        sa.get_highest_price_in_x_days(2, 20)
    with pytest.raises(TypeError):
        sa.get_highest_price_in_x_days("TSLA", "test")
    with pytest.raises(ValueError):
        sa.get_highest_price_in_x_days("TSLA", 0)
    with pytest.raises(ValueError):
        sa.get_highest_price_in_x_days("TSLA", -5)
