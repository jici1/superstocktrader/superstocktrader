from yahoo_fin import stock_info as si
from settings.settings import get_settings

def get_current_stock_value(stock_symbol):
    simulated = get_settings("settings/settings.json")["Simulation"]["SimulateMarketData"]
    #TODO this should not be read in file each time, it should be loaded in memory
    if not(isinstance(stock_symbol, str)):
        raise TypeError("Stock name should be string type")

    if (simulated):
        price = get_simulated_live_price(stock_symbol)
        if (price is not None):
            return price
    else:
        return si.get_live_price(stock_symbol)


def get_simulated_live_price(stock_symbol):
    return -1