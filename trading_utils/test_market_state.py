import pytest
from market_state import *

def test_market_is_opening():
    market_is_open = False
    new_market_is_open = True

    is_opening, is_closing  = get_market_state(market_is_open,new_market_is_open)
    assert is_opening == True
    assert is_closing == False

def test_market_is_closing():
    market_is_open = True
    new_market_is_open = False

    is_opening, is_closing  = get_market_state(market_is_open,new_market_is_open)
    assert is_opening == False
    assert is_closing == True


def test_market_is_still_closed():
    market_is_open = False
    new_market_is_open = False

    is_opening, is_closing  = get_market_state(market_is_open,new_market_is_open)
    assert is_opening == False
    assert is_closing == False

def test_market_is_still_open():
    market_is_open = True
    new_market_is_open = True

    is_opening, is_closing  = get_market_state(market_is_open,new_market_is_open)
    assert is_opening == False
    assert is_closing == False