from stock_data_api import *
import pytest

def test_get_stock_price():
    tsla_stock_price = get_current_stock_value("tsla")

    assert tsla_stock_price >= 0
    assert isinstance(tsla_stock_price, float)

    