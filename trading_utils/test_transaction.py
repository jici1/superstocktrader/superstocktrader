import pytest
from transaction import Transaction

def test_get_message():
    stock_symbol = "TSLA"
    number_of_stock = 241
    unit_price = 900

    trans = Transaction(True, stock_symbol, number_of_stock, unit_price)

    msg = trans.get_message("test_reporter")

    assert type(msg) == str

    if not msg:
        pytest.fail("empty message")


    assert ("bought" in msg) == True
    assert ("sold" in msg) == False
    assert (str(number_of_stock) in msg) == True
    assert (str(unit_price) in msg) == True
    assert (stock_symbol in msg) == True
