import pytest
from trader import Trader
from settings.settings import get_settings

settings_for_test = get_settings("settings/settings_tests_stock_trading_manager.json")
trader_settings = settings_for_test.get("Traders")[0]


def test_construction():
    test_trader = Trader(trader_settings)

    assert test_trader.Reporter != None
    assert test_trader.Portfolio != None