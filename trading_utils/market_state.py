from datetime import date, time, datetime
import holidays

def market_is_open(current_time = None, current_date = None):
    open_time = time(9,30)
    close_time = time(16,00)

    now_time = datetime.now().time() if current_time is None else current_time
    today = date.today() if current_date is None else current_date

    if now_time >= open_time and now_time <= close_time:
        # 0: monday, 1: tuesday , 2: wednesday, 3: thursday, 4: friday
        if (today.weekday() < 5):
            us_holidays = holidays.UnitedStates()
            if(today not in us_holidays):
               return True

    return False

def get_market_state(is_open,new_check_is_open):
    if(new_check_is_open and not(is_open)):
        is_opening = True
        is_closing = False

    elif(not(new_check_is_open) and is_open):
        is_closing = True
        is_opening = False

    else:
        is_opening, is_closing = False, False
        
    return is_opening, is_closing


def make_market_date_time(SimulationTime):

    market_hour = SimulationTime["SimulationStartHour"]
    market_min = SimulationTime["SimulationStartMinutes"]
    market_day = SimulationTime["SimulationDateDay"]
    market_month = SimulationTime["SimulationDateMonth"]
    market_year = SimulationTime["SimulationDateYear"]


    market_time = time(int(market_hour), int(market_min))
    market_date = date(int(market_year), int(market_month), int(market_day))

    return market_time, market_date

        
def update_simulated_time(last_time, last_date, begin_time):
    now = datetime.now()
    diff = now - begin_time

    new_time  = datetime.combine(last_date, last_time) + diff
    return new_time.time(), new_time.date()