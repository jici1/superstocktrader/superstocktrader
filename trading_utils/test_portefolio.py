import pytest
from portfolio import Portfolio
from stock_data_api import get_current_stock_value

def test_buy_stock():

    test_portfolio = Portfolio('empty_path','test')
    test_portfolio.cash = 1_000_000_000
    
    test_portfolio.buy_stock('GOOG', 10)

    assert test_portfolio.cash == 1_000_000_000 - (get_current_stock_value('GOOG') * 10)
    assert len(test_portfolio.current_stocks) == 1

    with pytest.raises(ValueError):
        test_portfolio.buy_stock('FB', -5)
    
    with pytest.raises(TypeError):
        test_portfolio.buy_stock( 5, -10)

    with pytest.raises(TypeError):
        test_portfolio.buy_stock( 'GOOG', 'GOOG')

def test_update_owned_stock():

    test_portfolio = Portfolio('empty_path','test')
    test_portfolio.cash = 1_000_000_000
    
    test_portfolio.buy_stock('TSLA', 10)

    assert len(test_portfolio.current_stocks) == 1

    test_portfolio.buy_stock('TSLA', 20)

    assert len(test_portfolio.current_stocks) == 1

    assert test_portfolio.current_stocks[0].number_of_stock == 30

def test_sell_stock():

    test_portfolio = Portfolio('empty_path','test')
    test_portfolio.cash = 1_000_000_000

    test_portfolio.buy_stock('GOOG', 10)
    test_portfolio.buy_stock('TSLA', 10)
    test_portfolio.buy_stock('FB', 10)
    assert len(test_portfolio.current_stocks) == 3

    test_portfolio.sell_stock('GOOG', 10)
    assert len(test_portfolio.current_stocks) == 2
    
    assert pytest.approx(test_portfolio.cash, 0.01) == 1_000_000_000 - (get_current_stock_value('FB')*10) - (get_current_stock_value('TSLA')*10)

    test_portfolio.sell_stock('FB', 5)
    assert len(test_portfolio.current_stocks) == 2

    for stock in test_portfolio.current_stocks:
        if stock.stock_symbol == 'FB':
            assert stock.number_of_stock == 5

    with pytest.raises(ValueError):
        test_portfolio.sell_stock('TSLA', -10)
    
    with pytest.raises(TypeError):
        test_portfolio.sell_stock( 5, -10)

    with pytest.raises(TypeError):
        test_portfolio.sell_stock('TSLA', 'TSLA')

def test_get_portfolio_value():

    test_portfolio = Portfolio('empty_path','test')
    test_portfolio.cash = 1_000_000_000

    test_portfolio.buy_stock('GOOG', 10)
    test_portfolio.buy_stock('TSLA', 10)

    stocks_val = get_current_stock_value('GOOG')*10 + get_current_stock_value('TSLA')*10

    assert test_portfolio.get_portfolio_value() == pytest.approx(stocks_val, rel =0.01)

def test_is_in_portfolio():
    test_portfolio = Portfolio('empty_path','test')
    test_portfolio.cash = 1_000_000_000
    
    test_portfolio.buy_stock('GOOG', 10)

    assert test_portfolio.is_in_portfolio("GOOG") == True  
    assert test_portfolio.is_in_portfolio("TSLA") == False  
  
