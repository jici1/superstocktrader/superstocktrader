from trading_utils.stock import Stock
from trading_utils.stock_data_api import get_current_stock_value
from utils.file_operations import read_json, write_json
import os

class Portfolio:
    def __init__(self, portfolio_path, trader_name):
        self.portfolio_path = portfolio_path
        self.trader_name = trader_name
        self.current_stocks = []
        self.cash = 0
    
    def load_portfolio(self):
        if not(os.path.exists(self.portfolio_path)):
            return

        portfolio =  read_json(self.portfolio_path)["Portfolio"][0]
        self.cash = portfolio.get("Cash")
        stocks = portfolio.get("Stocks")
        for stock in stocks:
            curr_stock = Stock(stock.get("Stock_symbol"),stock.get("Number_of_stock"), stock.get("Average_stock_value"))
            self.current_stocks.append(curr_stock)

    def save_portfolio(self):
        out_json = {}
        out_json["Portfolio"] = []

        json_portfolio = self.make_json_portfolio()
        out_json["Portfolio"].append(json_portfolio)

        write_json(self.portfolio_path, out_json)

    def make_json_portfolio(self):
        json_str = {
            "Trader" : self.trader_name ,
            "Cash": self.cash,
            "Stocks": []
        }
        for stock in self.current_stocks:
            json_str["Stocks"].append(stock.jsonify())
       
        return json_str

    def buy_stock(self, stock_symbol, number_of_stock):
        self.__check_arguments_validity__(stock_symbol)
        
        stock_value = get_current_stock_value(stock_symbol)
        transaction_value = stock_value * number_of_stock
        if self.cash >= transaction_value:
            self.increment_stock_nb(stock_symbol, number_of_stock, stock_value)
            self.cash -= transaction_value
        else:
            print('You do not have enough cash in your portfolio')

    def sell_stock(self, stock_symbol, number_of_stock):
        self.__check_arguments_validity__(stock_symbol)

        if not self.is_in_portfolio(stock_symbol):
           print("You do not own that stock")

        stock_value = get_current_stock_value(stock_symbol)
        transaction_value = stock_value * number_of_stock
        self.decrement_stock_nb(stock_symbol, number_of_stock, stock_value)
        self.cash += transaction_value

    def is_in_portfolio(self, stock_symbol):
        for stock in self.current_stocks:
            if stock.stock_symbol == stock_symbol:
                return True
        
        return False

    def increment_stock_nb(self, stock_symbol, number_of_stock, stock_value):
        self.__check_arguments_validity__(stock_symbol, number_of_stock , stock_value)

        if number_of_stock > 0:
            if self.is_in_portfolio(stock_symbol):
                for stock in self.current_stocks:
                    if stock.stock_symbol == stock_symbol:
                            self.update_owned_stock('buy', stock_symbol, number_of_stock, stock_value)
            else:
                self.add_new_stock(stock_symbol, number_of_stock, stock_value)
        else:
            raise ValueError("Cannot have a negative stock number")

    def decrement_stock_nb(self, stock_symbol, number_of_stock, stock_value):
        self.__check_arguments_validity__(stock_symbol, number_of_stock, stock_value)

        if number_of_stock > 0:
            for stock in self.current_stocks:
                if stock.stock_symbol == stock_symbol:
                    if stock.number_of_stock < number_of_stock:
                        raise ValueError("Cannot sell more stock than you own")
                    elif stock.number_of_stock == number_of_stock:
                        self.current_stocks.remove(stock)
                    else:
                        self.update_owned_stock('sell', stock.stock_symbol, -1*number_of_stock, stock_value)
        else:
            raise ValueError("Cannot have a negative stock number")

    def update_owned_stock(self, transaction_type, stock_symbol, number_new_stock, new_stock_value):
        self.__check_arguments_validity__(stock_symbol, number_new_stock, new_stock_value)

        for stock in self.current_stocks:
            if stock.stock_symbol == stock_symbol:
                stock.update_stock_attributes(transaction_type, number_new_stock, new_stock_value)
            
    def add_new_stock(self, stock_symbol, number_of_stock, stock_value):
        self.__check_arguments_validity__(stock_symbol, number_of_stock, stock_value)

        self.current_stocks.append(Stock(stock_symbol, number_of_stock, stock_value))

    def get_portfolio_value(self):
        value = 0
        for stock in self.current_stocks:
            value += stock.number_of_stock * get_current_stock_value(stock.stock_symbol)

        return value

    def __check_arguments_validity__(self,stock_symbol, number_of_stock=0, stock_value=0.0, transaction_value=0.0):
        if not(isinstance(stock_symbol, str)):
            raise TypeError("stock name should be string type")

        if not(isinstance(number_of_stock, int)):
            raise TypeError("number_of_stock name should be int type")

        if not(isinstance(stock_value, float)):
            raise TypeError("stock_value should be float type")

        if not(isinstance(transaction_value, float)):
            raise TypeError("transaction_value should be float type")

    def on_new_transaction(self, transaction):
        if (transaction.buy):
            self.buy_stock(transaction.stock_symbol, transaction.number_of_stocks)
        else:
            self.sell_stock(transaction.stock_symbol, transaction.number_of_stocks)

        self.cash -= transaction.fees
        
        self.save_portfolio()

