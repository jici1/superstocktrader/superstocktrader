from trading_utils.transaction import Transaction
from trading_utils.Reporter.SMTP import *
import os
from datetime import datetime
from utils.file_operations import write_to_text_file

class Reporter():
    def __init__(self, report_settings, trader_name):
        self.report_settings = report_settings
        self.current_logging_file = None
        self.trader_name = trader_name

    def on_new_transaction(self, transaction):
        if transaction is None:
            return
            
        message = transaction.get_message(self.trader_name)

        if (self.report_settings.get("LogOnAllTransaction")):
            self.log(message)

        if (self.report_settings.get("EmailOnAllTransaction")):
            self.send_email(message)

        if (self.report_settings.get("PrintAllTransaction")):
            print(message)

    def send_email(self, transaction):
        event_mail = Email(self.report_settings.get("SMTP"),transaction) 
        event_mail.send_transaction_mail()

    def log(self, log_msg):
        file_path = self.report_settings.get("LoggingDirectory")
        file_txt = "transaction_log_for_" + str(datetime.date(datetime.now())) + ".txt"
        self.current_logging_file = os.path.join(file_path, file_txt)

        if not os.path.exists(self.current_logging_file):
            write_to_text_file(self.current_logging_file ,log_msg, False)

        else:
            write_to_text_file(self.current_logging_file ,log_msg)

    def on_market_closing(self):
        self.log("Market Closing.")

    def on_market_opening(self):
        self.log("Market Opening!")