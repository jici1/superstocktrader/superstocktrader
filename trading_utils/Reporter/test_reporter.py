import pytest
from reporter import *
from utils.file_operations import read_text_file, remove_last_line_from_text_file

settings_for_test = {
                  "LogOnAllTransaction": False,
                  "EmailOnAllTransaction:": False,
                  "EmailOnMarketClosing":True,
                  "LoggingDirectory": "data/log"
               }

def test_constructor():
    reporter_name = "test_reporter"
    rep = Reporter(settings_for_test, reporter_name)
    assert rep.report_settings.get("LogOnAllTransaction") == False
    assert rep.report_settings.get("EmailOnMarketClosing") == True
    assert rep.report_settings.get("LoggingDirectory") == "data/log"

def test_log():
    reporter_name = "test_reporter"
    str_to_write = "this is a test!"
    rep = Reporter(settings_for_test, reporter_name)
    rep.log(str_to_write)

    log_text = read_text_file(rep.current_logging_file)

    #assert str_to_write == log_text

    #deleting the last line
    remove_last_line_from_text_file(rep.current_logging_file)

