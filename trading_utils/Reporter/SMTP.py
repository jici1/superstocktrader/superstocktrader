import smtplib, ssl

class Email():
    def __init__(self, SMTPSettings, transaction = None):
        self.port = SMTPSettings.get("Port")
        self.SMTP_server = SMTPSettings.get("SMTP_server")
        self.sender_email = SMTPSettings.get("sender_email")
        self.sender_password = SMTPSettings.get("sender_password")
        self.receivers = SMTPSettings.get("receivers")
        self.transaction = transaction

    def send_transaction_mail(self, message):

        self.send_mail(message, "Transaction Occured!")


    def send_daily_report(self, message):

        self.send_mail(message, "Daily Top Stocks Report")


    def send_mail(self, message, subject):

        message = 'Subject: {}\n\n{}'.format(subject, message)
         
        context = ssl.create_default_context()
        
        server = self.connect_to_server()
        is_connected = self.is_connected(server)
        connection_attemp = 1

        while(not(is_connected) and connection_attemp <=3 ):
            server = self.connect_to_server()
            is_connected = self.is_connected(server)
            connection_attemp+=1

        server.starttls(context=context)
        server.login(self.sender_email, self.sender_password)
        for receiver in self.receivers:
            server.sendmail(self.sender_email, receiver, message)
        
        server.quit()

    def connect_to_server(self):
        server = smtplib.SMTP(self.SMTP_server, self.port)
        return server
    
    def is_connected(self, server):
        try:
            status = server.noop()[0]
        except:  # smtplib.SMTPServerDisconnected
            status = -1
        return status == 250


