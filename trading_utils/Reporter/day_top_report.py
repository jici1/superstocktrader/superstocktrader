from trading_utils.Stock_Analyst.stock_analyst import get_top_gainers, get_top_losers
from utils.file_operations import write_to_text_file
from settings.settings import get_settings
from trading_utils.Reporter.SMTP import Email
import pandas as pd
from pandas.tseries.offsets import DateOffset
from datetime import datetime
import os
import yahoo_fin as si

class DayTopReport():

    def __init__(self):
        self.settings = get_settings("settings/settings.json")
        self.day_top_report_settings = self.settings.get("DayTopReport")
        self.day_top_report_path = self.day_top_report_settings.get("DayTopReportPath")
        self.top_gainers_setting = self.day_top_report_settings.get("TopGainers")
        self.top_losers_settings = self.day_top_report_settings.get("TopLosers")
        self.watched_stocks = self.day_top_report_settings.get("StockToWatch")
        self.top_gainers_data_base = get_top_gainers(self.top_gainers_setting)
        self.top_losers_data_base = get_top_losers(self.top_losers_settings)

        mail_sender = Email(self.settings["DayTopReport"]["SMTP"])

        file_txt = "day_top_report_" + str(datetime.date(datetime.now())) + ".txt"
        file_message = self.data_base_to_string()
        self.current_logging_file = os.path.join(self.day_top_report_path, file_txt)

        if not os.path.exists(self.current_logging_file):
            write_to_text_file(self.current_logging_file , file_message, False)

        else:
            write_to_text_file(self.current_logging_file ,file_message)

        if  self.day_top_report_settings["SendEmail"]:
            mail_sender.send_daily_report(self.data_base_to_string(True))
    
    def data_base_to_string(self, short = False):        
        
        if short:
            column = ['Symbol', 'Price (Intraday)', '% Change']
        else:
            column = None

        top_gainers_string = self.top_gainers_data_base.to_string(index=False, columns=column)
        top_losers_string = self.top_losers_data_base.to_string(index=False, columns=column)

        text_file = "TOP GAINERS\n" + top_gainers_string + "\n\nTOP LOSERS\n" + top_losers_string
        text_file += self.make_watched_stocks_string()

        return text_file
    
    def make_watched_stocks_string(self):
        text ="\n\n\n" + "WATCHED STOCKS % VALUE CHANGE\n"
        text+= 'Symbol\tDaily\tLast week\tLast month\tLast 3 months\n'

        for stock in self.watched_stocks:
            text+= self.make_stock_string(stock)
        return text

    def make_stock_string(self, stock):
        last_closing_price = si.stock_info.get_live_price(stock)

        three_months_ago = pd.Timestamp('today') - DateOffset(months=3)
        today = pd.to_datetime("today")

        closing_prices = si.stock_info.get_data(stock, start_date = three_months_ago, end_date = today )["close"]

        txt = stock
        txt += "\t "+ str(self.get_variation_string(last_closing_price,closing_prices[-1]))
        txt += "\t "+ str(self.get_variation_string(last_closing_price, closing_prices[-5]))
        txt += "\t\t\t"+ str(self.get_variation_string(last_closing_price, closing_prices[-21]))
        txt += "\t\t   "+ str(self.get_variation_string(last_closing_price, closing_prices[0]))
        return txt+"\n"

    def get_variation_string(self, last_price, old_price):
        var = self.get_variation(last_price, old_price)
        return("{:.2f}".format(round(var, 2)))

    def get_variation(self, last_price, old_price):
        var = 100*(last_price-old_price)/old_price
        return var


    