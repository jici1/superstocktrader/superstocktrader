from trading_utils.Reporter.reporter import Reporter
from trading_utils.portfolio import Portfolio
from trading_algos.trading_algo import TradingAlgo

class Trader():
    def __init__(self, settings):
        self.name = settings.get("Name")
        self.StockToWatch = settings.get("StockToWatch")
        self.Algo_to_use = settings.get("Algo_to_use")
        self.Reporter = Reporter(settings.get("Reporter_settings"), self.name)
        self.Portfolio = Portfolio(settings.get("portfolio_path"), settings.get("Name"))
        self.transaction_fees = settings.get("transaction_fees")

    def init(self):
        self.Portfolio.load_portfolio()

    def trade(self, is_opening, is_closing):
        if (is_closing):
            self.on_market_closing()

        if (is_opening):
            self.on_market_opening()
            
        #strategy design pattern
        for algo in self.Algo_to_use:
            curr_algo = TradingAlgo(self.name, algo, self.Portfolio, self.StockToWatch, self.transaction_fees)
            curr_algo.run_algo()
            if (curr_algo.transaction_required()):
                self.on_new_transaction(curr_algo.get_transaction())

    #observer design pattern
    def on_new_transaction(self, transaction):
        self.Reporter.on_new_transaction(transaction)
        self.Portfolio.on_new_transaction(transaction)

    def on_market_opening(self):
        self.Reporter.on_market_opening()

    def on_market_closing(self):
        self.Reporter.on_market_closing()


