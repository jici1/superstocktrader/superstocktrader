from datetime import datetime
from trading_utils.stock_data_api import get_current_stock_value

class Transaction():
    def __init__(self, buy, stock_symbol, number_of_stocks, unit_price = None, fees = 0, ID = -1):
        self.transaction_ID = ID
        self.buy = buy
        self.number_of_stocks = number_of_stocks
        self.stock_symbol = stock_symbol
        self.unit_price = unit_price
        self.get_unit_price()
        self.fees = fees
        self.date_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        
    def get_unit_price(self):
        if self.unit_price is None:
            self.unit_price = get_current_stock_value(self.stock_symbol)

    def get_message(self, trader_name):
        msg = "Transaction occured for trader " + trader_name + "! \n"

        if (self.buy):
            trans_type = "bought"
        else:
            trans_type = "sold"

        msg += str(self.number_of_stocks) + " stocks " + trans_type + " of " + self.stock_symbol 
        msg += " at unit price of " + "{:.2f}".format(self.unit_price) + " $ for a total of " + "{:.2f}".format(self.number_of_stocks * self.unit_price) + " $. "
        msg += "Transaction ID: " + str(self.transaction_ID) + " at " + str(self.date_time)
        msg+= "\n"
        
        return msg