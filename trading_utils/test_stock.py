import pytest
from stock import Stock

def test_get_purchase_value():

    test_stock = Stock("GOOG", 10, 1410.00)

    assert test_stock.get_purchase_value() == 14100.00
    assert test_stock.get_purchase_value(10, 200.00) == 2000.00

def test_update_stock_attributes():

    test_stock = Stock("GOOG", 10, 1410.00)

    test_stock.update_stock_attributes('buy', 20, 1500.00)

    assert test_stock.stock_symbol == "GOOG"
    assert test_stock.number_of_stock == 30
    assert test_stock.average_stock_value == 1470.00
    assert test_stock.get_purchase_value() == 44100.00

    test_stock.update_stock_attributes('sell', -15, 2000.00)

    assert test_stock.number_of_stock == 15
    assert test_stock.average_stock_value == 1470.00
    assert test_stock.get_purchase_value() == 22050.00

    with pytest.raises(ValueError):
        test_stock.update_stock_attributes(1 , 10, 200.00)

    with pytest.raises(ValueError):
        test_stock.update_stock_attributes('test' , 10, 200.00)
    
    with pytest.raises(TypeError):
        test_stock.update_stock_attributes('buy', 'test', 200.00)

    with pytest.raises(TypeError):
        test_stock.update_stock_attributes('buy', 10.5, 200.00)
    
    with pytest.raises(TypeError):
        test_stock.update_stock_attributes('buy', 10, 'test')