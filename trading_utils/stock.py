import json 
class Stock:
    def __init__(self, stock_symbol, number_of_stock, average_stock_value):
        self.stock_symbol = stock_symbol
        self.number_of_stock = number_of_stock
        self.average_stock_value = average_stock_value

    def get_purchase_value(self, number_of_stocks=None, stock_value=None):

        if stock_value is None and number_of_stocks is None:
            return self.number_of_stock * self.average_stock_value

        return stock_value * number_of_stocks
    
    def update_stock_attributes(self,transaction_type, number_new_stock, new_stock_value):
        self.__check_arguments_validity__(transaction_type, number_new_stock, new_stock_value)

        if transaction_type == 'buy':
            self.average_stock_value = (self.get_purchase_value() + self.get_purchase_value(number_new_stock, new_stock_value)) / (self.number_of_stock + number_new_stock)
        self.number_of_stock += number_new_stock

    def jsonify(self):
        json_str = {
                "Stock_symbol": self.stock_symbol,
                "Number_of_stock": self.number_of_stock,
                "Average_stock_value": self.average_stock_value
            }
        
        return json_str

    def __check_arguments_validity__(self, transaction_type, number_new_stock, new_stock_value):

        if not transaction_type in ['buy','sell']:
            raise ValueError("The transaction_type should be buy or sell")

        if not(isinstance(new_stock_value, float)):
            raise TypeError("stock_value should be float type")

        if not(isinstance(number_new_stock, int)):
            raise TypeError("number_new_stock should be int type")
