1) install virtual env:
pip install virtualenv==20.0.21

2) create virtual env 
virtualenv stock_trading

3) activate:
on windows:
stock_trading\Scripts\activate

on linux:
source stock_trading/bin/activate


4) pip install -r setup/requirements.txt

5) in visual studio code, select the proper python environment pointing to ...\superstocktrader\stock_trading\Scripts\python.exe

6) when asked by visual studio code, say yes to install pylint

7) add your root dir to path in environnment variable ex. for me:
C:\Users\jcd\Desktop\jc_dev\superstocktrader

Notes:
to deactivate:
deactivate

As long as your virtual environment is activated pip will install packages into that specific environment and you’ll be able to 
import and use packages in your Python application.
