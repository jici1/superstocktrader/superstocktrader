from datetime import datetime, time

def time_string_to_datetime(time_string):
    # HH:MM format
    time_hh_mm = time_string.split(":")
    hh = int(time_hh_mm[0])
    mm = int(time_hh_mm[1])

    return time(hh,mm)


