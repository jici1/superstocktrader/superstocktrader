import os 
import json

def read_json(json_file_path):
    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as fp:
            try:
                file = json.load(fp)
                return file
            except Exception as e:
                print("Exception {0} reading file {1}".format(e , json_file_path))
    else:
        raise Exception("Json file not found in path {0}".format(json_file_path)) 

def write_json(json_file_path, object_to_write):
    if os.path.exists(json_file_path):
        with open(json_file_path, 'w') as fp:
            try:
                json.dump(object_to_write,fp, indent=4)
            except Exception as e:
                print("Exception {0} reading file {1}".format(e , json_file_path))
    else:
        raise Exception("Json file not found in path {0}".format(json_file_path)) 

def read_text_file(txt_file_path):
    try:
        with open(txt_file_path) as log_file:
            return log_file.read()
    except Exception as e:
        print("Exception {0} writing to file {1}".format(e , txt_file_path))

def write_to_text_file(txt_file_path, message, append = True):
    file_path = os.path.dirname(txt_file_path)

    if not os.path.exists(file_path):
        os.mkdir(file_path)

    if append:
        operation = "a"
    else:
        operation = "w"
    try:
        with open(txt_file_path, operation) as log_file:
            log_file.write(message)
    except Exception as e:
        print("Exception {0} writing to file {1}".format(e , txt_file_path))

def remove_last_line_from_text_file(txt_file_path):
    try:
        with open(txt_file_path) as log_file:
            lines =  log_file.readlines()
            log_file.close()
            
        lines = lines[:-1]
        with open(txt_file_path, "w") as log_file:
            log_file.writelines([[item for item in lines[:-1]]])
            log_file.close()

    except Exception as e:
        print("Exception {0} writing to file {1}".format(e , txt_file_path))

def change_algo_settings_in_json(file_path, curr_algo, setting, new_value):
    algo_settings = read_json(file_path)
    for algo in algo_settings["algo_settings"]:
        if algo["name"] == curr_algo:
            algo[setting] = new_value
    write_json(file_path, algo_settings)