from trading_utils.transaction import Transaction
from trading_utils.stock_data_api import *
from datetime import datetime, time, timedelta
from settings.settings import get_settings
from utils.datetime_operations import time_string_to_datetime
from utils.file_operations import change_algo_settings_in_json, read_json
from trading_utils.Stock_Analyst.stock_analyst import price_is_lowest_in_x_days

class TradingAlgo():
    def __init__(self, trader_name, algo_name, portfolio, stock_to_watch, transaction_fees):
        self.transaction = None
        self.trader_name = trader_name
        self.algo_name = algo_name
        self.stock_to_watch = stock_to_watch
        self.portfolio = portfolio
        self.transaction_fees = transaction_fees
        self.algo_settings = self.get_current_algo_settings()

    def transaction_required(self):
        return self.transaction is not None

    def get_transaction(self):
        return self.transaction

    def make_transaction(self, buy, stock_symbol, number_of_stocks, unit_price = None):
        self.transaction = Transaction(buy, stock_symbol, number_of_stocks, unit_price, self.transaction_fees)

    def get_current_cash(self):
        return self.portfolio.cash

    def get_algos_path(self):
        settings = get_settings("settings/settings.json")
        traders = settings.get("Traders")

        for trader in traders:
            if trader["Name"] == self.trader_name:
                return trader["algos_path"]

    def get_current_algo_settings(self):
        settings = read_json(self.get_algos_path())["algo_settings"]
        curr_algo_settings = []

        for algos in settings:
            if algos["name"] == self.algo_name:
                return algos

    def current_stock_in_portfolio(self):
        return self.portfolio.current_stocks

    def run_algo(self):
        if self.algo_name == "Lowest_30_days":
            self.lowest_30_days()
        if self.algo_name == "Noon_trade":
            self.noon_trade()
        if self.algo_name == "Buy_Lowest_x_Days":
            self.buy_lowest_x_days()

    def buy_lowest_x_days(self):
        for stock in self.algo_settings["stocks_to_buy"]:
            stock_symbol = stock["stock_symbol"]
            number_of_stocks = stock["number_of_stocks"]
            x_days = stock["x_days"]
            bought = stock["bought"]

            if price_is_lowest_in_x_days(stock_symbol, x_days) and not bought:
                stock_price = get_current_stock_value(stock_symbol)
                self.make_transaction(True, stock_symbol, number_of_stocks, stock_price)

    def noon_trade(self):
        now = datetime.now().time()

        buy_time = time_string_to_datetime(self.algo_settings["buy_time"])
        sell_time = time_string_to_datetime(self.algo_settings["sell_time"])
        stock_symbol = self.stock_to_watch[0]

        if (now > buy_time and now < sell_time and not(self.algo_settings["has_been_executed"])):
            #Buy
            stock_price = get_current_stock_value(stock_symbol)
            nb_stock = int((self.get_current_cash() - self.transaction_fees) // stock_price)
            if nb_stock > 0:
                self.make_transaction(True, stock_symbol, nb_stock, stock_price)
                change_algo_settings_in_json(self.get_algos_path(), "Noon_trade", "has_been_executed", True)


        elif(now > sell_time and self.algo_settings.get("has_been_executed")):
            #sell
            nb_stock = 0
            stock_price = get_current_stock_value(stock_symbol)
            for stock in self.portfolio.current_stocks:
                if stock.stock_symbol == stock_symbol:
                    nb_stock = stock.number_of_stock

            if nb_stock > 0:
                self.make_transaction(False, stock_symbol, nb_stock, stock_price)
                change_algo_settings_in_json(self.get_algos_path(), "Noon_trade", "has_been_executed", False)

     
